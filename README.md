Requirements:
1. Set environment variables:
   * "TOKEN" for Telegram token
   * "IP_ADDRESS" for IP address to monitor
   * "CHAT_ID" for Telegram chat which will get the messages
99. Give permissions after building and before running the app 'sudo setcap cap_net_raw=eip ./target/debug/power'

---
A lot of deployment code was copypasted, I feel like 90% of fly.toml is redundant.

---
For deployment:
1. Set up secrets (replace "***" with relevant values)
```
flyctl secrets set TOKEN="***" CHAT_ID="***" IP_ADDRESS="***"
```
2. Build docker image and deploy
```
DOCKER_BUILDKIT=1 flyctl deploy --local-only
```

Thanks to fly.io team for hosting!