use std::env;
use std::str::FromStr;
use std::fmt::Debug;
use lazy_static::lazy_static;

pub struct MyEnv {
    // secrets
    pub chat_id: i64,
    pub telegram_token: String,
    pub monitor_ips: Vec<String>,
    // non-secret
    pub idle_threshold: i32,
}

lazy_static! {
    pub static ref ENV: MyEnv = MyEnv {
        chat_id: get_num("CHAT_ID"),
        telegram_token: env::var("TOKEN").unwrap(),
        monitor_ips: get_vec("IP_ADDRESS"),
        //
        idle_threshold: get_num_or("IDLE_THRESHOLD", "2"),
    };
}

fn get_vec(env_name: &str) -> Vec<String> {
    env::var(env_name)
        .unwrap()
        .split(',')
        .map(|s| s.to_string())
        .collect()
}

fn get_num<T>(env_name: &str) -> T where
    T: FromStr,
    T::Err: Debug,
{ env::var(env_name).unwrap().parse().unwrap() }

fn get_num_or<T>(env_name: &str, default: &str) -> T where
    T: FromStr,
    T::Err: Debug,
{ env::var(env_name).unwrap_or(default.to_string()).parse().unwrap() }