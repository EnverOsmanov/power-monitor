use std::sync::mpsc::Receiver;

use fastping_rs::{Pinger, PingResult};

use config::ENV;

use crate::config;

pub fn build_pinger() -> (Pinger, Receiver<PingResult>) {
    let (pinger, results) = match Pinger::new(Some(60 * 1000 as u64), Some(56)) {
        Ok((pinger, results)) => (pinger, results),
        Err(e) => panic!("Error creating pinger: {}", e),
    };

    for ip in &ENV.monitor_ips {
        pinger.add_ipaddr(&ip);
    }
    (pinger, results)
}
