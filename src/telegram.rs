use frankenstein::{Api, ChatId};
use frankenstein::SendMessageParams;
use frankenstein::TelegramApi;

use config::ENV;

use crate::config;

pub fn send_message(text: &str) {
    let send_message_params = SendMessageParams::builder()
        .chat_id(ChatId::from(ENV.chat_id))
        .text(text)
        .build();

    let api = Api::new(&ENV.telegram_token);

    api.send_message(&send_message_params).unwrap();
}