#[macro_use]
extern crate log;
extern crate pretty_env_logger;

use std::collections::HashMap;
use std::sync::mpsc::Receiver;

use fastping_rs::PingResult;
use fastping_rs::PingResult::{Idle, Receive};

use pinger::build_pinger;
use state::{init_state, State, write_state};
use telegram::send_message;
use crate::config::ENV;

mod state;
mod telegram;
mod pinger;
mod config;

fn main() {
    pretty_env_logger::init();

    let (pinger, results) = build_pinger();

    pinger.run_pinger();

    handle_pongs(&results)
}

fn handle_pongs(results: &Receiver<PingResult>) {
    let mut idle_counts: HashMap<&String, i32> = ENV.monitor_ips
        .iter()
        .map(|ip| (ip, 0))
        .collect();

    loop {
        let state: State = init_state();

        match results.recv() {
            Ok(result) => match result {
                Idle { addr } => {
                    if let Some(idle_count) = idle_counts.get_mut(&addr.to_string()) {
                        *idle_count += 1;

                        debug!("⚠️ {}. Idle count: {}", addr, idle_count);
                    }

                    // 1) let's ignore 1st idle response (due to internet provider issues etc.)
                    // 2) when pings from all IPs are idle - no power in the house.
                    // If at least 1 responds, then power is not gone.
                    if idle_counts.values().all(|n| ENV.idle_threshold <= *n) {
                        let text = "Power is DOWN ❌️";

                        consider_save_and_notify(&state, &text, false)
                    };
                }
                Receive { addr, rtt: _rtt } => {
                    if let Some(idle_count) = idle_counts.get_mut(&addr.to_string()) {
                        *idle_count = 0;

                        let text = "Power is UP ⚡";

                        consider_save_and_notify(&state, &text, true);
                    }
                }
            },
            Err(_) => panic!("Worker threads disconnected before the solution was found!"),
        };
    }
}

fn consider_save_and_notify(state: &State, text: &&str, new_is_up: bool) {
    if !contains_bool(state.is_up, new_is_up) {
        let new_state = State { is_up: Some(new_is_up) };

        write_state(&new_state);

        send_message(&text);
    };

    info!("{}", text);
}


// todo: remove when 'option_result_contains' will be stable
fn contains_bool(bool_o: Option<bool>, other: bool) -> bool {
    match bool_o {
        Some(b) => b == other,
        None => false
    }
}