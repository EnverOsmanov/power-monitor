use std::fs;
use std::path::Path;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct State {
    pub is_up: Option<bool>,
}

const FILENAME: &str = "power_persist/state.json";

pub fn init_state() -> State {

    fn create() {
        write_state(&State { is_up: None })
    }

    if !Path::new(&FILENAME).exists() { create() };

    let str = fs::read(&FILENAME).unwrap();
    let state = serde_json::from_slice(&str).unwrap_or(State { is_up: None });

    state
}


pub fn write_state(new_state: &State) {
    let json = serde_json::to_string_pretty(&new_state).unwrap();
    fs::write(&FILENAME, json).unwrap();
}